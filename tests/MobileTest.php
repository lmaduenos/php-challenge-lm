<?php

namespace Tests;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use App\Contact;
use App\Mobile;
use App\Call;
use App\Sms;
use App\Interfaces\CarrierInterface;
use App\Services\ContactService;

class Movistar implements CarrierInterface {

    public function dialContact(Contact $contact){
        return '';
	}

	public function makeCall($number) : Call{
        return new Call(400);
	}

	public function makeSms($number, $body) : Sms{
        return new Sms(400);
    }
}

class MobileTest extends TestCase
{
	
	/** @test */
	public function it_returns_null_when_name_empty()
	{
		$mobile = new Mobile(new Movistar);

		$this->assertNull($mobile->makeCallByName(''));
	}

	/** @test */
	public function it_returns_400_when_phone_is_off()
	{
		$mobile = new Mobile(new Movistar);		
		$this->assertSame(400, $mobile->makeCallByName('Luis Enrique')->status);
	}

	/** @test */
	public function it_returns_null_when_contact_does_exist()
	{
		$this->assertNull(ContactService::findByName('Luis Martín'));
	}

	/** @test */
	public function it_returns_400_when_sms_doesnt_send()
	{
		$mobile = new Mobile(new Movistar);
		$this->assertSame(400, $mobile->sendSmsByName('993033833', 'hello world')->status);
	}
}
