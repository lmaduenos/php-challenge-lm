<?php

namespace App\Services;

use App\Contact;


class ContactService
{
	
	public static function findByName($name)
	{
		// queries to the db
		$db_contacts_rows = array(
			new Contact('993033833','Luis Enrique'),
			new Contact('997764020','María Elena')
		);
		
		foreach($db_contacts_rows as $contact_row){
			if($contact_row->name == $name){				
				return $contact_row;
			}
		}
	}

	public static function validateNumber(string $number): bool
	{
		// logic to validate numbers
		if( empty($number) && strlen(number) < 9) return false;
		return true;
	}
}