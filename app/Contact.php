<?php

namespace App;


class Contact
{
	public $phone_number;
	public $name;
	
	function __construct($phone_number, $name)
	{
        $this->phone_number = $phone_number;
		$this->name = $name;
	}
	
	public function getPhoneNumber(){
        return $this->phone_number;
	}

	public function getName(){
        return $this->name;
	}
	
	public function setPhoneNumber($phone_number){
        $this->phone_number = $phone_number;
    }
	
	public function setName($name){
        $this->name = $name;
	}
}