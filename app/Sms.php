<?php

namespace App;

class Sms
{
	public $status;
	
	function __construct($status)
	{
        $this->status = $status;
	}
	
	public function getStatus(){
        return $this->status;
	}
	
	public function setStatus($status){
        $this->status = $status;
    }
}