<?php

namespace App\Interfaces;

use App\Contact;
use App\Call;
use App\Sms;

interface CarrierInterface
{
	
	public function dialContact(Contact $contact);

	public function makeCall($number): Call;

	public function makeSms($number, $body): Sms;
}